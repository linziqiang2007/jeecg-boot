package org.jeecg.modules.bkWenzhang.service.impl;

import org.jeecg.modules.bkWenzhang.entity.BkWenzhang;
import org.jeecg.modules.bkWenzhang.mapper.BkWenzhangMapper;
import org.jeecg.modules.bkWenzhang.service.IBkWenzhangService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 博客文章表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
@Service
public class BkWenzhangServiceImpl extends ServiceImpl<BkWenzhangMapper, BkWenzhang> implements IBkWenzhangService {

}
