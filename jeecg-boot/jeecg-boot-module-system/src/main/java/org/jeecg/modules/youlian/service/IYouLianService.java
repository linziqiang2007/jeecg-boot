package org.jeecg.modules.youlian.service;

import org.jeecg.modules.youlian.entity.YouLian;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 友情链接
 * @Author: jeecg-boot
 * @Date:   2022-06-27
 * @Version: V1.0
 */
public interface IYouLianService extends IService<YouLian> {

}
