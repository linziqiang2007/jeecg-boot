package org.jeecg.modules.testboke.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.testboke.entity.TestBoke;

/**
 * @Description: 博客表
 * @Author: jeecg-boot
 * @Date:   2022-05-28
 * @Version: V1.0
 */
public interface ITestBokeService extends IService<TestBoke> {

}
