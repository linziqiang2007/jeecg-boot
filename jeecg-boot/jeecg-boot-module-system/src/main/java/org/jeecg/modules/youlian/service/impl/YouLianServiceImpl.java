package org.jeecg.modules.youlian.service.impl;

import org.jeecg.modules.youlian.entity.YouLian;
import org.jeecg.modules.youlian.mapper.YouLianMapper;
import org.jeecg.modules.youlian.service.IYouLianService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 友情链接
 * @Author: jeecg-boot
 * @Date:   2022-06-27
 * @Version: V1.0
 */
@Service
public class YouLianServiceImpl extends ServiceImpl<YouLianMapper, YouLian> implements IYouLianService {

}
