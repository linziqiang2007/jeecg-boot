package org.jeecg.modules.youlian.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.youlian.entity.YouLian;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 友情链接
 * @Author: jeecg-boot
 * @Date:   2022-06-27
 * @Version: V1.0
 */
public interface YouLianMapper extends BaseMapper<YouLian> {

}
