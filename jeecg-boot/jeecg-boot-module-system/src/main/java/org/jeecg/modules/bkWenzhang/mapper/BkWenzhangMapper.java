package org.jeecg.modules.bkWenzhang.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.bkWenzhang.entity.BkWenzhang;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 博客文章表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
public interface BkWenzhangMapper extends BaseMapper<BkWenzhang> {

}
