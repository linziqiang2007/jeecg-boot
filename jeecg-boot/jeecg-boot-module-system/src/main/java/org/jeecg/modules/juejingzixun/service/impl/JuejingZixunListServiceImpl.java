package org.jeecg.modules.juejingzixun.service.impl;

import org.jeecg.modules.juejingzixun.entity.JuejingZixunList;
import org.jeecg.modules.juejingzixun.mapper.JuejingZixunListMapper;
import org.jeecg.modules.juejingzixun.service.IJuejingZixunListService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 资讯管理
 * @Author: jeecg-boot
 * @Date:   2022-06-10
 * @Version: V1.0
 */
@Service
public class JuejingZixunListServiceImpl extends ServiceImpl<JuejingZixunListMapper, JuejingZixunList> implements IJuejingZixunListService {

}
