import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
    {
    title: '创建人',
    align:"center",
    dataIndex: 'createBy'
   },
   {
    title: '分类名',
    align:"center",
    dataIndex: 'name'
   },
   {
    title: '分类描述',
    align:"center",
    dataIndex: 'content'
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '分类名',
    field: 'name',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入分类名!'},
          ];
     },
  },
  {
    label: '分类描述',
    field: 'content',
    component: 'Input',
  },
  {
    label: '父级节点',
    field: 'pid',
    component: 'Input',
  },
];
