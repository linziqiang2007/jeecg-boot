package org.jeecg.modules.youlian.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.youlian.entity.YouLian;
import org.jeecg.modules.youlian.service.IYouLianService;
import java.util.Date;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 友情链接
 * @Author: jeecg-boot
 * @Date:   2022-06-27
 * @Version: V1.0
 */
@Slf4j
@Api(tags="友情链接")
@RestController
@RequestMapping("/youlian/youLian")
public class YouLianController extends JeecgController<YouLian, IYouLianService> {
	@Autowired
	private IYouLianService youLianService;
	
	/**
	 * 分页列表查询
	 *
	 * @param youLian
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "友情链接-分页列表查询")
	@ApiOperation(value="友情链接-分页列表查询", notes="友情链接-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(YouLian youLian,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<YouLian> queryWrapper = QueryGenerator.initQueryWrapper(youLian, req.getParameterMap());
		Page<YouLian> page = new Page<YouLian>(pageNo, pageSize);
		IPage<YouLian> pageList = youLianService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param youLian
	 * @return
	 */
	@AutoLog(value = "友情链接-添加")
	@ApiOperation(value="友情链接-添加", notes="友情链接-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody YouLian youLian) {
		youLianService.save(youLian);
		return Result.OK("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param youLian
	 * @return
	 */
	@AutoLog(value = "友情链接-编辑")
	@ApiOperation(value="友情链接-编辑", notes="友情链接-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<?> edit(@RequestBody YouLian youLian) {
		youLianService.updateById(youLian);
		return Result.OK("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "友情链接-通过id删除")
	@ApiOperation(value="友情链接-通过id删除", notes="友情链接-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		youLianService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "友情链接-批量删除")
	@ApiOperation(value="友情链接-批量删除", notes="友情链接-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.youLianService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "友情链接-通过id查询")
	@ApiOperation(value="友情链接-通过id查询", notes="友情链接-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		YouLian youLian = youLianService.getById(id);
		return Result.OK(youLian);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param youLian
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, YouLian youLian) {
      return super.exportXls(request, youLian, YouLian.class, "友情链接");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, YouLian.class);
  }

}
