package org.jeecg.modules.bkWenzhang.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.bkWenzhang.entity.BkWenzhang;
import org.jeecg.modules.bkWenzhang.service.IBkWenzhangService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 博客文章表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
@Api(tags="博客文章表")
@RestController
@RequestMapping("/bkWenzhang/bkWenzhang")
@Slf4j
public class BkWenzhangController extends JeecgController<BkWenzhang, IBkWenzhangService> {
	@Autowired
	private IBkWenzhangService bkWenzhangService;
	
	/**
	 * 分页列表查询
	 *
	 * @param bkWenzhang
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "博客文章表-分页列表查询")
	@ApiOperation(value="博客文章表-分页列表查询", notes="博客文章表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<BkWenzhang>> queryPageList(BkWenzhang bkWenzhang,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<BkWenzhang> queryWrapper = QueryGenerator.initQueryWrapper(bkWenzhang, req.getParameterMap());
		Page<BkWenzhang> page = new Page<BkWenzhang>(pageNo, pageSize);
		IPage<BkWenzhang> pageList = bkWenzhangService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param bkWenzhang
	 * @return
	 */
	@AutoLog(value = "博客文章表-添加")
	@ApiOperation(value="博客文章表-添加", notes="博客文章表-添加")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody BkWenzhang bkWenzhang) {
		bkWenzhangService.save(bkWenzhang);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param bkWenzhang
	 * @return
	 */
	@AutoLog(value = "博客文章表-编辑")
	@ApiOperation(value="博客文章表-编辑", notes="博客文章表-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody BkWenzhang bkWenzhang) {
		bkWenzhangService.updateById(bkWenzhang);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "博客文章表-通过id删除")
	@ApiOperation(value="博客文章表-通过id删除", notes="博客文章表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		bkWenzhangService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "博客文章表-批量删除")
	@ApiOperation(value="博客文章表-批量删除", notes="博客文章表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.bkWenzhangService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "博客文章表-通过id查询")
	@ApiOperation(value="博客文章表-通过id查询", notes="博客文章表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<BkWenzhang> queryById(@RequestParam(name="id",required=true) String id) {
		BkWenzhang bkWenzhang = bkWenzhangService.getById(id);
		if(bkWenzhang==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(bkWenzhang);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param bkWenzhang
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, BkWenzhang bkWenzhang) {
        return super.exportXls(request, bkWenzhang, BkWenzhang.class, "博客文章表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, BkWenzhang.class);
    }

}
