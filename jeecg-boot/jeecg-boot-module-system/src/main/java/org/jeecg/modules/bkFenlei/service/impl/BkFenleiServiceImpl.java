package org.jeecg.modules.bkFenlei.service.impl;

import org.jeecg.modules.bkFenlei.entity.BkFenlei;
import org.jeecg.modules.bkFenlei.mapper.BkFenleiMapper;
import org.jeecg.modules.bkFenlei.service.IBkFenleiService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 博客资讯分类表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
@Service
public class BkFenleiServiceImpl extends ServiceImpl<BkFenleiMapper, BkFenlei> implements IBkFenleiService {

}
