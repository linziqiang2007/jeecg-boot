package org.jeecg.modules.bkWenzhang.service;

import org.jeecg.modules.bkWenzhang.entity.BkWenzhang;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 博客文章表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
public interface IBkWenzhangService extends IService<BkWenzhang> {

}
