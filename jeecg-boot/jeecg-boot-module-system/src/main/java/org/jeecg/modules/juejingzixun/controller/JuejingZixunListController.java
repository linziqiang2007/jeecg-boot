package org.jeecg.modules.juejingzixun.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.juejingzixun.entity.JuejingZixunList;
import org.jeecg.modules.juejingzixun.service.IJuejingZixunListService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.juejingzixuntype.entity.JuejingZixunType;
import org.jeecg.modules.juejingzixuntype.mapper.JuejingZixunTypeMapper;
import org.jeecg.modules.juejingzixuntype.service.IJuejingZixunTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 资讯管理
 * @Author: jeecg-boot
 * @Date:   2022-06-10
 * @Version: V1.0
 */
@Api(tags="资讯管理")
@RestController
@RequestMapping("/juejingzixun/juejingZixunList")
@Slf4j
public class JuejingZixunListController extends JeecgController<JuejingZixunList, IJuejingZixunListService> {
	@Autowired
	private IJuejingZixunListService juejingZixunListService;
	@Autowired
	private JuejingZixunTypeMapper juejingZixunTypeMapper;

	 @GetMapping(value = "/hello")
	 public Result<String> hello() {
		 Result<String> result = new Result<String>();
		 result.setResult("Hello World!");
		 result.setSuccess(true);
		 return result;
	 }
	/**
	 * 分页列表查询
	 *
	 * @param juejingZixunList
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "资讯管理-分页列表查询")
	@ApiOperation(value="资讯管理-分页列表查询", notes="资讯管理-分页列表查询")
	@GetMapping(value = {"/list/{type}","/list"})
	public Result<IPage<JuejingZixunList>> queryPageList(JuejingZixunList juejingZixunList,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   @PathVariable(value = "type",required = false) String type,
								   HttpServletRequest req) {

		if (type != null){
			juejingZixunList.setType(type);
		}
		QueryWrapper<JuejingZixunList> queryWrapper = QueryGenerator.initQueryWrapper(juejingZixunList, req.getParameterMap());
		//LambdaQueryWrapper<JuejingZixunList> queryWrapper = new LambdaQueryWrapper<>();
		//queryWrapper.eq(JuejingZixunList::getType,type);
		Page<JuejingZixunList> page = new Page<JuejingZixunList>(pageNo, pageSize);
		IPage<JuejingZixunList> pageList = juejingZixunListService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	/**
	 *   添加
	 *
	 * @param juejingZixunList
	 * @return
	 */
	@AutoLog(value = "资讯管理-添加")
	@ApiOperation(value="资讯管理-添加", notes="资讯管理-添加")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody JuejingZixunList juejingZixunList) {
		juejingZixunListService.save(juejingZixunList);


		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param juejingZixunList
	 * @return
	 */
	@AutoLog(value = "资讯管理-编辑")
	@ApiOperation(value="资讯管理-编辑", notes="资讯管理-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody JuejingZixunList juejingZixunList) {
		juejingZixunListService.updateById(juejingZixunList);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "资讯管理-通过id删除")
	@ApiOperation(value="资讯管理-通过id删除", notes="资讯管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		juejingZixunListService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "资讯管理-批量删除")
	@ApiOperation(value="资讯管理-批量删除", notes="资讯管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.juejingZixunListService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "资讯管理-通过id查询")
	@ApiOperation(value="资讯管理-通过id查询", notes="资讯管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<JuejingZixunList> queryById(@RequestParam(name="id",required=true) String id) {
		JuejingZixunList juejingZixunList = juejingZixunListService.getById(id);
		if(juejingZixunList==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(juejingZixunList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param juejingZixunList
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, JuejingZixunList juejingZixunList) {
        return super.exportXls(request, juejingZixunList, JuejingZixunList.class, "资讯管理");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, JuejingZixunList.class);
    }

}
