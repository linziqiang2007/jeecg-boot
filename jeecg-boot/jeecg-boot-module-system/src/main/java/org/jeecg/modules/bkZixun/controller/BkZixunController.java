package org.jeecg.modules.bkZixun.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.bkZixun.entity.BkZixun;
import org.jeecg.modules.bkZixun.service.IBkZixunService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 博客资讯表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
@Api(tags="博客资讯表")
@RestController
@RequestMapping("/bkZixun/bkZixun")
@Slf4j
public class BkZixunController extends JeecgController<BkZixun, IBkZixunService> {
	@Autowired
	private IBkZixunService bkZixunService;
	
	/**
	 * 分页列表查询
	 *
	 * @param bkZixun
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "博客资讯表-分页列表查询")
	@ApiOperation(value="博客资讯表-分页列表查询", notes="博客资讯表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<BkZixun>> queryPageList(BkZixun bkZixun,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<BkZixun> queryWrapper = QueryGenerator.initQueryWrapper(bkZixun, req.getParameterMap());
		Page<BkZixun> page = new Page<BkZixun>(pageNo, pageSize);
		IPage<BkZixun> pageList = bkZixunService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param bkZixun
	 * @return
	 */
	@AutoLog(value = "博客资讯表-添加")
	@ApiOperation(value="博客资讯表-添加", notes="博客资讯表-添加")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody BkZixun bkZixun) {
		bkZixunService.save(bkZixun);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param bkZixun
	 * @return
	 */
	@AutoLog(value = "博客资讯表-编辑")
	@ApiOperation(value="博客资讯表-编辑", notes="博客资讯表-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody BkZixun bkZixun) {
		bkZixunService.updateById(bkZixun);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "博客资讯表-通过id删除")
	@ApiOperation(value="博客资讯表-通过id删除", notes="博客资讯表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		bkZixunService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "博客资讯表-批量删除")
	@ApiOperation(value="博客资讯表-批量删除", notes="博客资讯表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.bkZixunService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "博客资讯表-通过id查询")
	@ApiOperation(value="博客资讯表-通过id查询", notes="博客资讯表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<BkZixun> queryById(@RequestParam(name="id",required=true) String id) {
		BkZixun bkZixun = bkZixunService.getById(id);
		if(bkZixun==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(bkZixun);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param bkZixun
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, BkZixun bkZixun) {
        return super.exportXls(request, bkZixun, BkZixun.class, "博客资讯表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, BkZixun.class);
    }

}
