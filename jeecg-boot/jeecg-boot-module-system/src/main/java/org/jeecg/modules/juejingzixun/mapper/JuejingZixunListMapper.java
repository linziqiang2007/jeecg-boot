package org.jeecg.modules.juejingzixun.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.juejingzixun.entity.JuejingZixunList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 资讯管理
 * @Author: jeecg-boot
 * @Date:   2022-06-10
 * @Version: V1.0
 */
public interface JuejingZixunListMapper extends BaseMapper<JuejingZixunList> {

}
