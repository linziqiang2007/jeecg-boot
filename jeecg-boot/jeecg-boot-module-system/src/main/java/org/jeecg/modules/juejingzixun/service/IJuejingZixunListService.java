package org.jeecg.modules.juejingzixun.service;

import org.jeecg.modules.juejingzixun.entity.JuejingZixunList;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 资讯管理
 * @Author: jeecg-boot
 * @Date:   2022-06-10
 * @Version: V1.0
 */
public interface IJuejingZixunListService extends IService<JuejingZixunList> {

}
