import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
    {
    title: '创建人',
    align:"center",
    dataIndex: 'createBy'
   },
   {
    title: '创建日期',
    align:"center",
    dataIndex: 'createTime'
   },
   {
    title: '更新人',
    align:"center",
    dataIndex: 'updateBy'
   },
   {
    title: '更新日期',
    align:"center",
    dataIndex: 'updateTime'
   },
   {
    title: '标题',
    align:"center",
    dataIndex: 'title'
   },
   {
    title: '摘要',
    align:"center",
    dataIndex: 'subTitle'
   },
   {
    title: '文章图片',
    align:"center",
    dataIndex: 'titleImg',
    customRender:render.renderAvatar,
   },
   {
    title: '作者',
    align:"center",
    dataIndex: 'author'
   },
   {
    title: '资讯分类',
    align:"center",
    dataIndex: 'type_dictText'
   },
   {
    title: '阅读量',
    align:"center",
    dataIndex: 'redingNum'
   },
   {
    title: '文章内容',
    align:"center",
    dataIndex: 'content',
    slots: { customRender: 'htmlSlot' },
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '标题',
    field: 'title',
    component: 'Input',
  },
  {
    label: '摘要',
    field: 'subTitle',
    component: 'Input',
  },
  {
    label: '文章图片',
    field: 'titleImg',
     component: 'JImageUpload',
     componentProps:{
      },
  },
  {
    label: '作者',
    field: 'author',
    component: 'Input',
  },
  {
    label: '资讯分类',
    field: 'type',
    component: 'JDictSelectTag',
    componentProps:{
        dictCode:"juejing_zixun_type,name,id"
     },
  },
  {
    label: '阅读量',
    field: 'redingNum',
    component: 'InputNumber',
  },
  {
    label: '文章内容',
    field: 'content',
    component: 'JCodeEditor', //TODO String后缀暂未添加
  },
];
