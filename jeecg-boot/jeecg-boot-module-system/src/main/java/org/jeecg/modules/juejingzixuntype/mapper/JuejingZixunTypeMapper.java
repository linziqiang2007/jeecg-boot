package org.jeecg.modules.juejingzixuntype.mapper;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.juejingzixuntype.entity.JuejingZixunType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 资讯分类
 * @Author: jeecg-boot
 * @Date:   2022-06-10
 * @Version: V1.0
 */
public interface JuejingZixunTypeMapper extends BaseMapper<JuejingZixunType> {

	/**
	 * 编辑节点状态
	 * @param id
	 * @param status
	 */
	void updateTreeNodeStatus(@Param("id") String id,@Param("status") String status);

}
