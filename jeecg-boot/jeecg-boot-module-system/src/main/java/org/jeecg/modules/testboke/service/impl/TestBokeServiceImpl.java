package org.jeecg.modules.testboke.service.impl;

import org.jeecg.modules.testboke.entity.TestBoke;
import org.jeecg.modules.testboke.mapper.TestBokeMapper;
import org.jeecg.modules.testboke.service.ITestBokeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 博客表
 * @Author: jeecg-boot
 * @Date:   2022-05-28
 * @Version: V1.0
 */
@Service
public class TestBokeServiceImpl extends ServiceImpl<TestBokeMapper, TestBoke> implements ITestBokeService {

}
