package org.jeecg.modules.testboke.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.testboke.entity.TestBoke;

/**
 * @Description: 博客表
 * @Author: jeecg-boot
 * @Date:   2022-05-28
 * @Version: V1.0
 */
public interface TestBokeMapper extends BaseMapper<TestBoke> {

}
