package org.jeecg.modules.bkZixun.service;

import org.jeecg.modules.bkZixun.entity.BkZixun;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 博客资讯表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
public interface IBkZixunService extends IService<BkZixun> {

}
