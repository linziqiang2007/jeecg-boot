package org.jeecg.modules.bkZixun.service.impl;

import org.jeecg.modules.bkZixun.entity.BkZixun;
import org.jeecg.modules.bkZixun.mapper.BkZixunMapper;
import org.jeecg.modules.bkZixun.service.IBkZixunService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 博客资讯表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
@Service
public class BkZixunServiceImpl extends ServiceImpl<BkZixunMapper, BkZixun> implements IBkZixunService {

}
