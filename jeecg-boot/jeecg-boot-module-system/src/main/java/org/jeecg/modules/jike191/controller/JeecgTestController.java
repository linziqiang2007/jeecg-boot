package org.jeecg.modules.jike191.controller;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auth Created by Administrator on 2022/5/22.
 */
@RestController
@RequestMapping("/test/jeecgDemo2")
@Slf4j
public class JeecgTestController {
    /**
     * hello world
     *
     * @param
     * @return
     */
    @GetMapping(value = "/hello2")
    public Result<String> hello() {

        System.out.println("执行方法hello2");

        Result<String> result = new Result<String>();

        result.setResult("Hello World!");
        result.setSuccess(true);
        return result;
    }

    /**
     * hello world
     *
     * @param
     * @return
     */
    @GetMapping(value = "/hello3")
    public Result<String> hello3() {
        System.out.println("执行方法hello3");
        Result<String> result = new Result<String>();
        String returnMsg = "Hello World3!";
        result.setResult(returnMsg);
        result.setSuccess(true);
        return result;
    }
}
