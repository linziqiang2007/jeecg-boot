package org.jeecg.modules.juejingzixuntype.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.juejingzixun.entity.JuejingZixunList;
import org.jeecg.modules.juejingzixuntype.entity.JuejingZixunType;
import org.jeecg.modules.juejingzixuntype.service.IJuejingZixunTypeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 资讯分类
 * @Author: jeecg-boot
 * @Date:   2022-06-10
 * @Version: V1.0
 */
@Api(tags="资讯分类")
@RestController
@RequestMapping("/juejingzixuntype/juejingZixunType")
@Slf4j
public class JuejingZixunTypeController extends JeecgController<JuejingZixunType, IJuejingZixunTypeService>{
	@Autowired
	private IJuejingZixunTypeService juejingZixunTypeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param juejingZixunType
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "资讯分类-分页列表查询")
	@ApiOperation(value="资讯分类-分页列表查询", notes="资讯分类-分页列表查询")
	@GetMapping(value = "/rootList")
	public Result<IPage<JuejingZixunType>> queryPageList(JuejingZixunType juejingZixunType,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String hasQuery = req.getParameter("hasQuery");
		//JuejingZixunList juejingZixunList = new JuejingZixunList();
        if(hasQuery != null && "true".equals(hasQuery)){
            QueryWrapper<JuejingZixunType> queryWrapper =  QueryGenerator.initQueryWrapper(juejingZixunType, req.getParameterMap());
            List<JuejingZixunType> list = juejingZixunTypeService.queryTreeListNoPage(queryWrapper);
            IPage<JuejingZixunType> pageList = new Page<>(1, 50, list.size());
            pageList.setRecords(list);
            return Result.OK(pageList);
        }else{
            String parentId = juejingZixunType.getPid();
            if (oConvertUtils.isEmpty(parentId)) {
                parentId = "0";
            }
            juejingZixunType.setPid(null);
            QueryWrapper<JuejingZixunType> queryWrapper = QueryGenerator.initQueryWrapper(juejingZixunType, req.getParameterMap());
            // 使用 eq 防止模糊查询
            queryWrapper.eq("pid", parentId);
            Page<JuejingZixunType> page = new Page<JuejingZixunType>(pageNo, pageSize);
            IPage<JuejingZixunType> pageList = juejingZixunTypeService.page(page, queryWrapper);
            return Result.OK(pageList);
        }
	}

	 /**
      * 获取子数据
      * @param juejingZixunType
      * @param req
      * @return
      */
	//@AutoLog(value = "资讯分类-获取子数据")
	@ApiOperation(value="资讯分类-获取子数据", notes="资讯分类-获取子数据")
	@GetMapping(value = "/childList")
	public Result<IPage<JuejingZixunType>> queryPageList(JuejingZixunType juejingZixunType,HttpServletRequest req) {
		QueryWrapper<JuejingZixunType> queryWrapper = QueryGenerator.initQueryWrapper(juejingZixunType, req.getParameterMap());
		List<JuejingZixunType> list = juejingZixunTypeService.list(queryWrapper);
		IPage<JuejingZixunType> pageList = new Page<>(1, 10, list.size());
        pageList.setRecords(list);
		return Result.OK(pageList);
	}

    /**
      * 批量查询子节点
      * @param parentIds 父ID（多个采用半角逗号分割）
      * @return 返回 IPage
      * @param parentIds
      * @return
      */
	//@AutoLog(value = "资讯分类-批量获取子数据")
    @ApiOperation(value="资讯分类-批量获取子数据", notes="资讯分类-批量获取子数据")
    @GetMapping("/getChildListBatch")
    public Result getChildListBatch(@RequestParam("parentIds") String parentIds) {
        try {
            QueryWrapper<JuejingZixunType> queryWrapper = new QueryWrapper<>();
            List<String> parentIdList = Arrays.asList(parentIds.split(","));
            queryWrapper.in("pid", parentIdList);
            List<JuejingZixunType> list = juejingZixunTypeService.list(queryWrapper);
            IPage<JuejingZixunType> pageList = new Page<>(1, 10, list.size());
            pageList.setRecords(list);
            return Result.OK(pageList);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("批量查询子节点失败：" + e.getMessage());
        }
    }
	
	/**
	 *   添加
	 *
	 * @param juejingZixunType
	 * @return
	 */
	@AutoLog(value = "资讯分类-添加")
	@ApiOperation(value="资讯分类-添加", notes="资讯分类-添加")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody JuejingZixunType juejingZixunType) {
		juejingZixunTypeService.addJuejingZixunType(juejingZixunType);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param juejingZixunType
	 * @return
	 */
	@AutoLog(value = "资讯分类-编辑")
	@ApiOperation(value="资讯分类-编辑", notes="资讯分类-编辑")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody JuejingZixunType juejingZixunType) {
		juejingZixunTypeService.updateJuejingZixunType(juejingZixunType);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "资讯分类-通过id删除")
	@ApiOperation(value="资讯分类-通过id删除", notes="资讯分类-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		juejingZixunTypeService.deleteJuejingZixunType(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "资讯分类-批量删除")
	@ApiOperation(value="资讯分类-批量删除", notes="资讯分类-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.juejingZixunTypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "资讯分类-通过id查询")
	@ApiOperation(value="资讯分类-通过id查询", notes="资讯分类-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<JuejingZixunType> queryById(@RequestParam(name="id",required=true) String id) {
		JuejingZixunType juejingZixunType = juejingZixunTypeService.getById(id);
		if(juejingZixunType==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(juejingZixunType);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param juejingZixunType
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, JuejingZixunType juejingZixunType) {
		return super.exportXls(request, juejingZixunType, JuejingZixunType.class, "资讯分类");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, JuejingZixunType.class);
    }

}
