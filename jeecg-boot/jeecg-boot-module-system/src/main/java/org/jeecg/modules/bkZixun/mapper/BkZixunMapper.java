package org.jeecg.modules.bkZixun.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.bkZixun.entity.BkZixun;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 博客资讯表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
public interface BkZixunMapper extends BaseMapper<BkZixun> {

}
