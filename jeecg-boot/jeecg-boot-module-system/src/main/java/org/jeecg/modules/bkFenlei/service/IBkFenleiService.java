package org.jeecg.modules.bkFenlei.service;

import org.jeecg.modules.bkFenlei.entity.BkFenlei;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 博客资讯分类表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
public interface IBkFenleiService extends IService<BkFenlei> {

}
