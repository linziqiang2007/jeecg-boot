package org.jeecg.modules.bkWenzhang.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 博客文章表
 * @Author: jeecg-boot
 * @Date:   2022-06-13
 * @Version: V1.0
 */
@Data
@TableName("bk_wenzhang")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="bk_wenzhang对象", description="博客文章表")
public class BkWenzhang implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private String id;
	/**作者*/
    @ApiModelProperty(value = "作者")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**博客文章标题*/
	@Excel(name = "博客文章标题", width = 15)
    @ApiModelProperty(value = "博客文章标题")
    private String tittle;
	/**博客文章内容*/
	@Excel(name = "博客文章内容", width = 15)
    @ApiModelProperty(value = "博客文章内容")
    private String bktext;
	/**文章摘要*/
	@Excel(name = "文章摘要", width = 15)
    @ApiModelProperty(value = "文章摘要")
    private String zhaiyao;
	/**博客分类*/
	@Excel(name = "博客分类", width = 15)
    @ApiModelProperty(value = "博客分类")
    private String bkFenlei;
	/**分类ID*/
	@Excel(name = "分类ID", width = 15)
    @ApiModelProperty(value = "分类ID")
    private String fenleiId;
	/**封面图片*/
	@Excel(name = "封面图片", width = 15)
    @ApiModelProperty(value = "封面图片")
    private String fengmian;
	/**是否删除：1代表删除*/
	@Excel(name = "是否删除：1代表删除", width = 15)
    @ApiModelProperty(value = "是否删除：1代表删除")
    private Integer delFlag;
	/**是否置顶*/
	@Excel(name = "是否置顶", width = 15)
    @ApiModelProperty(value = "是否置顶")
    private String bkZhiding;
	/**点击量*/
	@Excel(name = "点击量", width = 15)
    @ApiModelProperty(value = "点击量")
    private Integer readCount;
}
